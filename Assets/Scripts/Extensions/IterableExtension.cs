﻿/* FPV City View
 * Copyright (C) 2022 -- Capaldi12 -- artem.shebarshov.1@gmail.com
 * 
 * Расширения для последовательностей.
 */


using System.Collections.Generic;
using System.Linq;


namespace Extensions.IterableExtension
{
    public static class IterableExtension
    {
        // Пропустить последние n элементов списка
        public static IEnumerable<T> SkipLast<T>(this IEnumerable<T> list, int n = 1)
        {
            return list.Take(list.Count() - n);
        }

        public static IEnumerable<(int, T)> Enumerate<T>(this IEnumerable<T> list, int start = 0)
        {
            return list.Select((v, i) => (start + i, v));
        }
    }
}
