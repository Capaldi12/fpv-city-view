﻿/* FPV City View
 * Copyright (C) 2022 -- Capaldi12 -- artem.shebarshov.1@gmail.com
 * 
 * Расширения для числовых типов.
 */


namespace Extensions.NumberExtension
{
    public static class NumberExtension
    {
        // Взять ближайшее "целое" - число меньше a кратное b
        public static double Whole(this double a, double b)
        {
            return a - (a % b);
        }

        public static float Whole(this float a, float b)
        {
            return a - (a % b);
        }

        // Преобразовать число градусов в кортеж из градусов и угловых минут и секунд
        public static (int deg, int min, double sec) ToDMS(this double c)
        {
            var deg = (int)c.Whole(1);

            c = (c % 1) * 60;
            var min = (int)c.Whole(1);

            var sec = (c % 1) * 60;

            return (deg, min, sec);
        }
    }
}
