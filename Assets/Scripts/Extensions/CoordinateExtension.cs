﻿/* FPV City View
 * Copyright (C) 2022 -- Capaldi12 -- artem.shebarshov.1@gmail.com
 * 
 * Расширения саязанные с классом GeoCoordinate.
 */


using GeoCoordinatePortable;

using MapObjects;


namespace Extensions.CoordinateExtension
{
    // Расширения связанные с координатами
    public static class CoordinateExtenstion
    {
        // Координаты по широте и долготе сущности из бд

        public static GeoCoordinate Coordinate(this Node node)
        {
            return new GeoCoordinate(node.Lat, node.Lon);
        }

        public static GeoCoordinate Coordinate(this Building building)
        {
            return new GeoCoordinate(building.Lat, building.Lon);
        }

        public static GeoCoordinate Coordinate(this Chunk chunk)
        {
            return new GeoCoordinate(chunk.Lat, chunk.Lon);
        }

        // Координата с заменой одной из компонент

        public static GeoCoordinate WithLatitude(this GeoCoordinate g, double latitude)
        {
            return new GeoCoordinate(latitude, g.Longitude, g.Altitude);
        }

        public static GeoCoordinate WithLongitude(this GeoCoordinate g, double longitude)
        {
            return new GeoCoordinate(g.Latitude, longitude, g.Altitude);
        }

        public static GeoCoordinate WithAltitude(this GeoCoordinate g, double altitude)
        {
            return new GeoCoordinate(g.Latitude, g.Longitude, altitude);
        }

        // Прибавление значений к каждой компоненте
        public static GeoCoordinate Add(this GeoCoordinate g, double latitude = 0, double longitude = 0, double altitude = 0)
        {
            return new GeoCoordinate(g.Latitude + latitude, g.Longitude + longitude, g.Altitude + altitude);
        }
    }
}
