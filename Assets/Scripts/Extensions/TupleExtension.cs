﻿/* FPV City View
 * Copyright (C) 2022 -- Capaldi12 -- artem.shebarshov.1@gmail.com
 * 
 * Расширения для работы с кортежами.
 */


using System;

using Extensions.NumberExtension;


namespace Extensions.TupleExtension
{
    public static class TupleExtension
    {
        // Строковое представление координат в виде градусов
        public static string AsDD(this (double lat, double lon) c)
        {
            var latMark = c.lat > 0 ? "N" : "S";
            var lonMark = c.lon > 0 ? "E" : "W";

            var lat = Math.Abs(c.lat);
            var lon = Math.Abs(c.lon);

            return $"{lat:0.#####}°{latMark} {lon:0.#####}°{lonMark}";
        }

        // Строковое представление координат в виде градусов и угловых минут и секунд
        public static string AsDMS(this (double lat, double lon) c)
        {
            var latMark = c.lat > 0 ? "N" : "S";
            var lonMark = c.lon > 0 ? "E" : "W";

            var lat = Math.Abs(c.lat).ToDMS();
            var lon = Math.Abs(c.lon).ToDMS();

            return $"{lat.deg}°{lat.min}'{lat.sec:0.##}\"{latMark} {lon.deg}°{lon.min}'{lon.sec:0.##}\"{lonMark}";
        }
    }
}
