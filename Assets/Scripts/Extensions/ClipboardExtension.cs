﻿/* FPV City View
 * Copyright (C) 2022 -- Capaldi12 -- artem.shebarshov.1@gmail.com
 * 
 * Расширения строк для работы с буффером обмена. 
 */


using UnityEngine;


namespace Extensions.ClipboardExtension
{
    public static class ClipboardExtension
    {
        // Копирование в буфер обмена
        public static void CopyToClipboard(this string str)
        {
            GUIUtility.systemCopyBuffer = str;
        }
    }
}
