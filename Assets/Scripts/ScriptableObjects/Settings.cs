﻿/* FPV City View
 * Copyright (C) 2022 -- Capaldi12 -- artem.shebarshov.1@gmail.com
 * 
 * Настройки симуляции.
 */


using System.IO;

using UnityEngine;


[CreateAssetMenu(fileName = "Settings", menuName = "ScriptableObjects/Settings", order = 1)]
public class Settings : ScriptableObject
{
    // Папка для временных файлов карты
    public string MapFolder => Application.temporaryCachePath;

    // Название файла БД
    [Header("Files")]
    public string DBFileName = "lite.db";
    // Полный путь к файлу БД
    public string DBPath => Path.Combine(Application.persistentDataPath, DBFileName);

    // "Радиус" экспортруемого фрагмента
    [Header("Sizes")]
    public double ExportRadius = 0.01;
    // Получить границы экспортируемого фрагмента
    public (double minLon, double minLat, double maxLon, double maxLat) AsBBox((double lat, double lon) c)
    {
        return (c.lon - ExportRadius, c.lat - ExportRadius, c.lon + ExportRadius, c.lat + ExportRadius);
    }

    // Размер чанка
    public double ChunkSize = 0.005;
    // Смещение, которое гарантированно внутри чанка
    public double ChunkInside = 0.001;

    // Точность округления координат
    public int Precision = 3;

    [Header("Levels")]
    // Высота этажа в Unity
    public double LevelHeight = 3;
    // Радиус, в котором здания считаются близлежащими
    public double NearbyRadius = 0.002;
    // Количестово этажей по умолчанию
    public int DefaultLevels = 1;
}
