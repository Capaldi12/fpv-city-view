﻿/* FPV City View
 * Copyright (C) 2022 -- Capaldi12 -- artem.shebarshov.1@gmail.com
 * 
 * Настройки игрока/аватара.
 */


using UnityEngine;


// Настройки игрока
[CreateAssetMenu(fileName = "PlayerSettings", menuName = "ScriptableObjects/PlayerSettings", order = 2)]
public class PlayerSettings : ScriptableObject
{
    // Ускорение
    public float AccForce = 80;
    // Ускорение от реактивного ранца
    public float JetForce = 200;

    // Скорость шага
    public float MaxWalkSpeed = 2.5f;
    // Скорость бега
    public float MaxSprintSpeed = 3.8f;

    // Чуствительность мыши
    [Range(0.1f, 10)]
    public float MouseSensitivity = 0.5f;

    // Дальность прорисовки
    [Range(1, 7)]
    public int ViewDistance = 1;
}
