﻿/* FPV City View
 * Copyright (C) 2022  Денисов Валерий Андреевич  v_denissov@bk.ru
 * 
 * Объекты карты и база данных объектов карты.
 */


using System;
using System.Collections.Generic;
using System.Linq.Expressions;

using LiteDB;


namespace MapObjects
{
    public class Node
    {
        public long Id { get; set; }

        public double Lat { get; set; }
        public double Lon { get; set; }

        public override string ToString()
        {
            return $"Node[{Id}, at ({Lat}, {Lon})]";
        }
    }

    public class Building
    {
        public long Id { get; set; }

        [BsonRef("nodes")]
        public List<Node> Nodes { get; set; }

        public double Lat { get; set; }
        public double Lon { get; set; }
        public int Levels { get; set; }

        // Поиск зданий рядом от заданного
        public static Expression<Func<Building, bool>> Nearby(Building reference, double radius)
        {
            return b => b.Lat >= reference.Lat - radius
                     && b.Lat <= reference.Lat + radius
                     && b.Lon >= reference.Lon - radius
                     && b.Lon <= reference.Lon + radius;
        }

        public override string ToString()
        {
            return $"Building[{Id}, with {Nodes.Count} nodes and {Levels} levels]";
        }
    }

    public class Chunk
    {
        [BsonId]
        public ObjectId Id { get; set; }

        [BsonRef("buildings")]
        public List<Building> Buildings { get; set; }

        public double Lat { get; set; }
        public double Lon { get; set; }

        // Поиск чанка, расположенного по заданным координатам
        public static Expression<Func<Chunk, bool>> LocatedAt((double lat, double lon) c, Settings settings)
        {
            // Сравнение `==` плохо работает для чисел с плавающей точкой
            return Includes((c.lat + settings.ChunkInside, c.lon + settings.ChunkInside), settings);
        }

        // Поиск чанка, равного заданному
        public static Expression<Func<Chunk, bool>> EqualTo(Chunk c)
        {
            return ch => ch.Lat == c.Lat && ch.Lon == c.Lon;
        }

        // Поиск чанка, в котором находится заданная точка
        public static Expression<Func<Chunk, bool>> Includes((double lat, double lon) c, Settings settings)
        {
            return ch =>
            !(c.lat < ch.Lat || c.lat > ch.Lat + settings.ChunkSize ||
              c.lon < ch.Lon || c.lon > ch.Lon + settings.ChunkSize);
        }

        public override string ToString()
        {
            return $"Chunk[at ({Lat}, {Lon}) with {Buildings.Count} buildings]";
        }
    }

    public class Fragment
    {
        //[BsonRef("chunks")]
        //public List<Chunk> Chunks { get; set; }

        public double MinLon { get; set; } 
        public double MinLat { get; set; }
        public double MaxLon { get; set; }
        public double MaxLat { get; set; }

        public (double minLon, double minLat, double maxLon, double maxLat) BBox()
        {
            return (MinLon, MinLat, MaxLon, MaxLat);
        }

        // Поиск фрагмента, включающешл заданную точку
        public static Expression<Func<Fragment, bool>> Includes((double lat, double lon) c)
        {
            return fr => 
            !(c.lat < fr.MinLat || c.lat > fr.MaxLat || 
              c.lon < fr.MinLon || c.lon > fr.MaxLon);
        }

        // Поиск фрагмента с заданным ограничивающим прямоугольником
        public static Expression<Func<Fragment, bool>> WithBBox(
            (double minLon, double minLat, double maxLon, double maxLat) bbox
        )
        {
            return fr => fr.MinLon == bbox.minLon
                      && fr.MinLat == bbox.minLat
                      && fr.MaxLon == bbox.maxLon
                      && fr.MaxLat == bbox.maxLat;
        }

        public override string ToString()
        {
            return $"Fragment[{BBox()}]";  // with {Chunks.Count} chunks
        }
    }


    public class MapObjectsDB : IDisposable
    {
        private LiteDatabase db;

        public ILiteCollection<Building> Buildings => db.GetCollection<Building>("buildings");
        public ILiteCollection<Node> Nodes => db.GetCollection<Node>("nodes");
        public ILiteCollection<Chunk> Chunks => db.GetCollection<Chunk>("chunks");
        public ILiteCollection<Fragment> Fragments => db.GetCollection<Fragment>("fragments");
        public ILiteQueryable<Chunk> QueryChunks => 
            Chunks.Query().Include("$.Buildings").Include("$.Buildings[*].Nodes");

        public MapObjectsDB(string path)
        {
            db = new LiteDatabase(path);
        }

        public void Dispose()
        {
            db.Dispose();
            db = null;
        }

        public bool BeginTrans() => db.BeginTrans();
        public bool Commit() => db.Commit();
    }
}
