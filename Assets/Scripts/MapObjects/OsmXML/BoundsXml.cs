﻿/* FPV City View
 * Copyright (C) 2022  Денисов Валерий Андреевич  v_denissov@bk.ru
 * 
 * Границы участка карты, содержащегося в XML-файле.
 */


using System.Xml.Serialization;


namespace MapObjects.OsmXML
{
    // Границы участка карты, содержащегося в XML-файле
    [XmlRoot(ElementName = "bounds")]
    public class BoundsXml
    {
        [XmlAttribute(AttributeName = "minlat")]
        public string Minlat { get; set; }

        [XmlAttribute(AttributeName = "minlon")]
        public string Minlon { get; set; }

        [XmlAttribute(AttributeName = "maxlat")]
        public string Maxlat { get; set; }

        [XmlAttribute(AttributeName = "maxlon")]
        public string Maxlon { get; set; }

        public override string ToString()
        {
            return $"BoundsXml[{Minlat}, {Minlon}, {Maxlat}, {Maxlon}]";
        }
    }
}
