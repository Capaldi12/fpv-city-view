﻿/* FPV City View
 * Copyright (C) 2022  Денисов Валерий Андреевич  v_denissov@bk.ru
 * 
 * Узел - точка на карте с определёнными координатами.
 */


using System.Xml.Serialization;


namespace MapObjects.OsmXML
{
    // Узел/вершина пути
    [XmlRoot(ElementName = "node")]
    public class NodeXml : BaseElementXml
    {
        [XmlAttribute(AttributeName = "lat")]
        public string Lat { get; set; }


        [XmlAttribute(AttributeName = "lon")]
        public string Lon { get; set; }
    }
}
