﻿/* FPV City View
 * Copyright (C) 2022  Денисов Валерий Андреевич  v_denissov@bk.ru
 * 
 * Член отношения.
 */


using System.Xml.Serialization;


namespace MapObjects.OsmXML
{
    // Член отношения
    [XmlRoot(ElementName = "meber")]
    public class MemberXml
    {
        [XmlAttribute(AttributeName = "type")]
        public string Type { get; set; }


        [XmlAttribute(AttributeName = "ref")]
        public string Ref { get; set; }


        [XmlAttribute(AttributeName = "role")]
        public string Role { get; set; }
    }
}
