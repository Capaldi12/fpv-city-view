﻿/* FPV City View
 * Copyright (C) 2022  Денисов Валерий Андреевич  v_denissov@bk.ru
 * 
 * Корень osm xml документа. Используется для десереализации файла картографических данных.
 */


using System.Xml.Serialization;
using System.Collections.Generic;
using System.IO;


namespace MapObjects.OsmXML
{
    // Корень osm xml документа
    [XmlRoot(ElementName = "osm")]
    public class OsmXml
    {
        [XmlElement(ElementName = "bounds")]
        public BoundsXml Bounds { get; set; }


        [XmlElement(ElementName = "node")]
        public List<NodeXml> Nodes { get; set; }


        [XmlElement(ElementName = "way")]
        public List<WayXml> Ways { get; set; }


        [XmlElement(ElementName = "relation")]
        public List<RelationXml> Relations { get; set; }


        [XmlAttribute(AttributeName = "version")]
        public string Version { get; set; }


        [XmlAttribute(AttributeName = "generator")]
        public string Generator { get; set; }


        [XmlAttribute(AttributeName = "copyright")]
        public string Copyright { get; set; }


        [XmlAttribute(AttributeName = "attribution")]
        public string Attribution { get; set; }


        [XmlAttribute(AttributeName = "license")]
        public string License { get; set; }

        // Получить объект OsmXml из файла
        public static OsmXml fromFile(string filename)
        {
            OsmXml osm;

            using (var reader = new StreamReader(filename))
            {
                osm = (OsmXml)new XmlSerializer(typeof(OsmXml)).Deserialize(reader);
            }

            return osm;
        }
    }
}
