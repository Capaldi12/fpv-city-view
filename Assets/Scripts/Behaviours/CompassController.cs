﻿/* FPV City View
 * Copyright (C) 2022 -- Capaldi12 -- artem.shebarshov.1@gmail.com
 * 
 * Контроллер компаса. Управляет вращением стрелки компаса в интерфейсе.
 */


using UnityEngine;


// Виджет компаса
public class CompassController : MonoBehaviour
{
    [SerializeField]
    private Transform player;

    [SerializeField]
    private Transform arrow;

    void Update()
    {
        arrow.eulerAngles = new Vector3(0, 0, player.eulerAngles.y);
    }
}
