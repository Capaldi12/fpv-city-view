﻿/* FPV City View
 * Copyright (C) 2022 -- Capaldi12 -- artem.shebarshov.1@gmail.com
 * 
 * Контроллер инфо-дисплея. Управляет отображением информации
 * (в том числе отладочной) в интерфейсе программы.
 */


using System.Globalization;

using UnityEngine;
using TMPro;

using Extensions.TupleExtension;
using Extensions.ClipboardExtension;


// Управление информационным дисплеем
public class InformationDisplayController : MonoBehaviour
{
    [SerializeField]
    Settings settings;

    [SerializeField]
    GameObject debugScreen;

    [SerializeField]
    TextMeshProUGUI debugTextLeft;

    [SerializeField]
    TextMeshProUGUI debugTextRight;

    [SerializeField]
    PlayerController player;

    [SerializeField]
    CityController city;

    [SerializeField]
    Toaster toaster;

    private float fpsRefreshRate = .2f;
    private float fpsRefreshTimer;

    private int FPS = 60;
    private (double lat, double lon) playerGeoPosition;
    private Vector3 playerPosition;

    private string DD => $"{playerGeoPosition.AsDD()}";
    private string DMS => $"{playerGeoPosition.AsDMS()}";
    private string Elevation => $"{playerPosition.y:0.0}m {(int)(playerPosition.y / settings.LevelHeight)}L";

    private string FPSstr => $"{FPS,3} FPS";
    private string Position => $"{playerPosition.x:0.0} {playerPosition.y:0.0} {playerPosition.z:0.0}";

    void Start()
    {
        CultureInfo.DefaultThreadCurrentCulture = CultureInfo.InvariantCulture;
        CultureInfo.DefaultThreadCurrentUICulture = CultureInfo.InvariantCulture;

        debugScreen.SetActive(false);

        // player.DebugScreenToggled.AddListener(ToggleDebugScreen);
        // player.CoordinateCopyRequested.AddListener(CopyCoordinates);
    }

    void FixedUpdate()
    {
        playerGeoPosition = city.GetPlayerGeoPosition();
        playerPosition = player.transform.position;
    }

    void Update()
    {
        if (Time.unscaledTime > fpsRefreshTimer)
        {
            FPS = (int)(1f / Time.smoothDeltaTime);
            fpsRefreshTimer = Time.unscaledTime + fpsRefreshRate;
        }

        debugTextLeft.text = DD + "\n" + DMS + "\n" + Elevation + "\n";
        debugTextRight.text = FPSstr + "\n" + Position + "\n";
    }

    public void ToggleDebugScreen()
    {
        if (debugScreen.activeSelf)
            debugScreen.SetActive(false);
        else
            debugScreen.SetActive(true);
    }

    // Копирование текущей координаты игрока в буфер обмена
    public void CopyCoordinates(bool dms)
    {
        if (dms)
            DMS.CopyToClipboard();
        else
            DD.CopyToClipboard();

        toaster.ShowToast("Copied!", 3);
    }
}
