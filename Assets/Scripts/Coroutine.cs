/* FPV City View
 * Copyright (C) 2022 -- Capaldi12 -- artem.shebarshov.1@gmail.com
 * 
 * Корутина с поддержкой обработки искючений.
 */


using System;
using System.Collections;


namespace Coro 
{
    // Корутина с поддержкой обработки исключений
    public class Coroutine : IEnumerator
    {
        // Исключение, брошенное рутиной
        public Exception CurrentException { get; private set; } = null;

        // Рутина завершена
        public bool Finished { get; private set; } = false;
        

        private IEnumerator _routine;     // Внутрення рутина
        private Coroutine _child = null;  // Дочерняя рутина

        public Coroutine(IEnumerator baseRoutine)
        {
            _routine = baseRoutine;
        }

        public object Current { get; private set; } = null;

        public bool MoveNext()
        {
            if (_child != null) // Проверить наличие исключений у дочерней рутины
            {
                if (_child.CurrentException != null)
                {
                    // Передаём исключение как собственное
                    CurrentException = _child.CurrentException;
                    Finished = true;

                    return false;
                }

                // Сбросить дочернюю рутину
                _child = null;
            }

            try
            {
                var res = _routine.MoveNext(); // "Продвинуть" корутину
                Current = _routine.Current;   // Копиурем возвращенное значение

                if (res == false) 
                    Finished = true;  // Рутина завершена

                // Возвращённые IEnumerator становится дочерней корутиной
                else if (Current is IEnumerator)   
                    Current = _child = (Current as IEnumerator).Coroutine();
                
                return res; // Вернуть то, что вернула корутина
            }
            catch(Exception e)  // Поймать исключение, если такое произойдёт
            {
                CurrentException = e;
                Finished = true;
                return false; // Прервать выполнение
            }
        }
     
        // Не поддерживается корутиной
        public void Reset()
        {
            throw new NotSupportedException($"Coroutine does not support '{nameof(Reset)}' method");
        }
    }

    // Добавить IEnumerator-у возможность превратиться в корутину
    public static class CoroutineExtension
    {
        public static Coroutine Coroutine(this IEnumerator baseRoutine)
        {
            if (baseRoutine is Coroutine)
                return baseRoutine as Coroutine;

            return new Coroutine(baseRoutine);
        }
    }
}
