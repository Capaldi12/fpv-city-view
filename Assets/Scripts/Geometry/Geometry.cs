﻿/* FPV City View
 * Copyright (C) 2022 -- Capaldi12 -- artem.shebarshov.1@gmail.com
 * 
 * Различные геометрические функции.
 */


using System.Collections.Generic;

using UnityEngine;


namespace Geometry
{
    // Различные геометрицеские функции
    public static class GeometryFunctions
    {
        // Триангуляция полигона заданного списком точек
        // https://wiki.unity3d.com/index.php/Triangulator
        public static List<int> Triangulate(List<Vector3> points)
        {
            List<int> indices = new List<int>();

            int n = points.Count;

            if (n < 3)
                return indices;

            int[] V = new int[n];
            if (IsClockwise(points))
            {
                for (int v = 0; v < n; v++)
                    V[v] = v;
            }
            else
            {
                for (int v = 0; v < n; v++)
                    V[v] = (n - 1) - v;
            }

            int nv = n;
            int count = 2 * nv;
            for (int v = nv - 1; nv > 2;)
            {
                if ((count--) <= 0)
                    return indices;

                int u = v;
                if (nv <= u)
                    u = 0;
                v = u + 1;
                if (nv <= v)
                    v = 0;
                int w = v + 1;
                if (nv <= w)
                    w = 0;

                if (Snip(points, u, v, w, nv, V))
                {
                    int a, b, c, s, t;
                    a = V[u];
                    b = V[v];
                    c = V[w];
                    indices.Add(a);
                    indices.Add(b);
                    indices.Add(c);
                    for (s = v, t = v + 1; t < nv; s++, t++)
                        V[s] = V[t];
                    nv--;
                    count = 2 * nv;
                }
            }

            indices.Reverse();

            return indices;
        }

        // Площадь многоугольника
        // Знак результата показывает порядок, в котором располагаются точки
        //'+' - по часовой, '-' - против часовой
        static float Area(List<Vector3> points)
        {
            int n = points.Count;
            float A = 0.0f;

            for (int p = n - 1, q = 0; q < n; p = q++)
            {
                Vector3 pval = points[p];
                Vector3 qval = points[q];
                A += pval.x * qval.z - qval.x * pval.z;
            }

            return (A * 0.5f);
        }

        // Определяет, находятся ли точки в порядке обхода по часовой стрелке
        public static bool IsClockwise(List<Vector3> points)
        {
            return Area(points) > 0;
        }

        // Определяет корректность треугольника uvw
        private static bool Snip(List<Vector3> points, int u, int v, int w, int n, int[] V)
        {
            int p;
            Vector3 A = points[V[u]];
            Vector3 B = points[V[v]];
            Vector3 C = points[V[w]];
            if (Mathf.Epsilon > (((B.x - A.x) * (C.z - A.z)) - ((B.z - A.z) * (C.x - A.x))))
                return false;
            for (p = 0; p < n; p++)
            {
                if ((p == u) || (p == v) || (p == w))
                    continue;
                Vector3 P = points[V[p]];
                if (InsideTriangle(A, B, C, P))
                    return false;
            }
            return true;
        }

        // Определяет, находится ли точка внутри треугольника
        private static bool InsideTriangle(Vector3 A, Vector3 B, Vector3 C, Vector3 P)
        {
            float ax, az, bx, bz, cx, cz, apx, apz, bpx, bpz, cpx, cpz;
            float cCROSSap, bCROSScp, aCROSSbp;
               
            ax = C.x - B.x; az = C.z - B.z;
            bx = A.x - C.x; bz = A.z - C.z;
            cx = B.x - A.x; cz = B.z - A.z;
            apx = P.x - A.x; apz = P.z - A.z;
            bpx = P.x - B.x; bpz = P.z - B.z;
            cpx = P.x - C.x; cpz = P.z - C.z;

            aCROSSbp = ax * bpz - az * bpx;
            cCROSSap = cx * apz - cz * apx;
            bCROSScp = bx * cpz - bz * cpx;

            return ((aCROSSbp >= 0.0f) && (bCROSScp >= 0.0f) && (cCROSSap >= 0.0f));
        }

    }
}
