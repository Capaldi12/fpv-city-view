﻿/* FPV City View
 * Copyright (C) 2022 -- Capaldi12 -- artem.shebarshov.1@gmail.com
 * 
 * Специальный редактор свойств контроллера города.
 */


using UnityEngine;
using UnityEditor;
using System.IO;

namespace CustomEditors
{
    [CustomEditor(typeof(CityController))]
    public class CityControllerEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            CityController controller = target as CityController;

            DrawButtons(controller);
            
        }

        private void DrawButtons(CityController controller)
        {
            GUILayout.Space(10);

            using (new EditorGUI.DisabledScope(!Application.isPlaying))
            {
                if (GUILayout.Button("Reload Materials"))
                {
                    controller.ReloadMaterials();
                }
            }

            GUILayout.Space(30);

            using (new EditorGUI.DisabledScope(Application.isPlaying))
            {
                if (GUILayout.Button("Clear Database"))
                {
                    File.Delete(controller.DBPath);
                    Debug.Log("Database was cleared!");
                }

                EditorGUILayout.LabelField("Warning: this action cannot be undone!", EditorStyles.boldLabel);
            }
        }
    }
}